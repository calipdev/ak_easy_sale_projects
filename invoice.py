# -*- encoding: utf-8 -*-

from openerp.osv import fields, osv
from openerp import netsvc

class account_invoice(osv.Model):
    _inherit = 'account.invoice'

    _columns = {
        'num_fact_proyecto': fields.char('Factura', size=256, help='Numero de factura del total de Proyecto'),
    }
    
    def copy(self, cr, uid, id, default=None, context=None):
        res = super(account_invoice, self).copy(cr, uid, id, default, context)
        sale_order_obj = self.pool.get('sale.order')
        # read access on purchase.order object is not required
        if not sale_order_obj.check_access_rights(cr, uid, 'read', raise_exception=False):
            user_id = SUPERUSER_ID
        else:
            user_id = uid
        so_ids = sale_order_obj.search(cr, user_id, [('invoice_ids', 'in', id)], context=context)
        for order in sale_order_obj.browse(cr, user_id, so_ids, context=context):
            order.invoice_ids.append(res) 
        return res

    def confirm_paid(self, cr, uid, ids, context=None):
        res = super(account_invoice, self).confirm_paid(cr, uid, ids, context=context)
        sale_order_obj = self.pool.get('sale.order')
        invoice_obj = self.pool.get('account.invoice')
        so_ids = sale_order_obj.search(cr, uid, [('invoice_ids', 'in', ids)], context=context)
        flag = None
        if so_ids:
            for order in sale_order_obj.browse(cr, uid, so_ids, context=context):
                for invoice in invoice_obj.browse(cr, uid, [x.id for x in order.invoice_ids], context=context):
                    if invoice.state != 'paid':
                        flag = False
                        break
                    else:
                        flag = True
                if flag:
                    wf_service = netsvc.LocalService('workflow')
                    wf_service.trg_validate(uid, 'sale.order', order.id, 'all_lines', cr)
                flag = False            
        return res